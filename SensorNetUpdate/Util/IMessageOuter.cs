﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.Util
{
    /// <summary>
    ///  消息输出接口
    /// </summary>
    public interface IMessageOuter
    {
      void  ShowText(String text);
    }
}
