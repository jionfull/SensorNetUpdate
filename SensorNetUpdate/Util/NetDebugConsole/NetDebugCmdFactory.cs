﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.Util
{
    public  interface INetDebugCmdFactory
    {
        DelegateNetDebugCmd GetNetDebugCmdDelegate(String cmd);
        String[] GetCmdList();
    }
}
