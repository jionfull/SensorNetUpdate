﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;

namespace Lon.Util
{
    public class NetDebugConsoleTerminal
    {

        public bool connected = true;
        Thread rxThread;
        byte[] rxBuf = new byte[4096*128];
        Socket socket;
        string ip;
        int port;
        public String Ip
        {
            get
            {
                return ip;
            }
        }
        public Socket ClientSocket
        {
            get
            {
                return socket;
            }
        }
        StringBuilder cmdBuilder;

        public NetDebugConsoleTerminal(String ip):this(ip,4445)
        { 
        }
        public NetDebugConsoleTerminal(String ip,int port)
        {
           this.socket = new Socket(AddressFamily.InterNetwork,SocketType.Stream,ProtocolType.Tcp);
           this.ip = ip;
           this.port = port;
         
           
        }
        public void Init()
        {
            rxThread = new Thread(new ThreadStart(RxPorc));
            rxThread.IsBackground = true;
            rxThread.Start();
        }
        private void RxPorc()
        {
            IPAddress addr=null;
            try
            {
                addr = IPAddress.Parse(ip);
            }
            catch
            {
                Out(String.Format("IP[{0}]地址为非法的ip地址\r\n", ip));
            }

            try
            {
                while (true)
                {

                    try
                    {
                        Out("开始连接\r\n");
                        this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                        this.socket.Connect(IPAddress.Parse(ip), port);
                        Out("连接成功\r\n");
                        Reccive();
                    }
                    catch
                    {
                        Out("连接失败\r\n");
                        try
                        {
                            this.socket.Disconnect(true);
                        }
                        catch
                        { 
                        }
                        this.socket.Close();
                        Thread.Sleep(10000);
                    }
                    
                }
            }
            catch(Exception ex)
            {
                NetDebugConsole.WriteLine(ex.ToString()+ex.StackTrace);
            }
        }
        StringBuilder cacheSb = new StringBuilder();
        private void Reccive()
        {
            int count = 0;
            while (true)
            {

                if (this.socket.Poll(100, SelectMode.SelectRead))
                {
                    count += socket.Receive(rxBuf, count, rxBuf.Length - count, SocketFlags.None);
                }
                else
                {
                    String text = Encoding.Default.GetString(rxBuf, 0, count);
                    Out(text);
                    count = 0;
                }
            }
        }

        private void Out(string mess)
        {
            IMessageOuter temp = MessageOut;
            if (temp != null)
            {
                temp.ShowText(mess);
            }
            
        }
     

        /// <summary>
        /// 消息输出接口
        /// </summary>
        public IMessageOuter MessageOut;

        public void Send(string message)
        {
            try
            {
                this.socket.Send(Encoding.Default.GetBytes(message));
            }
            catch
            {
                
            }
        }
    }
}
