﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.Util
{
    public  class NetDebugCmd
    {
        private NetDebugConsoleClient requertClient;

        public NetDebugConsoleClient RequertClient
        {
            get { return requertClient; }
        }
        private String cmdStr;

        public String CmdStr
        {
            get { return cmdStr; }
            set { cmdStr = value; }
        }
        private String[] args;

        public String[] Args
        {
            get { return args; }
          
        }
        /// <summary>
        /// 调用前应保证cmd 不为空
        /// </summary>
        /// <param name="client"></param>
        /// <param name="cmd"></param>
        public NetDebugCmd(NetDebugConsoleClient client, String cmd)
        {
            this.requertClient = client;
            args = cmd.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            cmdStr = Args[0];
        }
    }

    public delegate void DelegateNetDebugCmd(NetDebugCmd cmd);
}
