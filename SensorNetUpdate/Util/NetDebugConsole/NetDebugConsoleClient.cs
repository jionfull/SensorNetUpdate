﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;

namespace Lon.Util
{
    public class NetDebugConsoleClient
    {

        public bool connected = true;
        Thread rxThread;
        byte[] rxBuf = new byte[4096];
        Socket socket;

        public String Ip
        {
            get
            {
                return ((IPEndPoint)socket.RemoteEndPoint).Address.ToString();
            }
        }
        public Socket ClientSocket
        {
            get
            {
                return socket;
            }
        }
     
        public NetDebugConsoleClient(Socket s)
        {
            this.socket = s;
           rxThread = new Thread(new ThreadStart(RxPorc));
           rxThread.IsBackground = true;
           rxThread.Start();
           
        }
        private void RxPorc()
        {
            try
            {
                while (true)
                {
                  
                   String cmd=ReadACmd();
                   NetDebugCmd nCmd = new NetDebugCmd(this, cmd);
                   NetDebugConsole.DoCmd(nCmd);
                    
                }
            }
            catch(Exception ex)
            {
                NetDebugConsole.WriteLine(ex.ToString()+ex.StackTrace);
            }
        }
        /// <summary>
        /// 读取完一条命令后剩余的数据
        /// </summary>
        byte[] noUseData=new byte[0];
        private string ReadACmd()
        {
            int  count=noUseData.Length;         
            Array.Copy(noUseData,rxBuf,noUseData.Length);
            for (int i = 0; i < count; i++)
            {
                if (rxBuf[i] == (byte)'\r')
                {
                    byte[] cmdbytes = new byte[i];
                    Array.Copy(rxBuf, cmdbytes, i);
                    noUseData = new byte[count - i - 1];
                    Array.Copy(rxBuf, i, noUseData, 0, count - i - 1);
                    String cmd= Encoding.Default.GetString(cmdbytes);
                    cmd = cmd.Trim();
                    return cmd;
                }
            }
            while (true)
            {
                count += socket.Receive(rxBuf, count, rxBuf.Length - count, SocketFlags.None);
                for (int i = 0; i < count; i++)
                {
                    if (rxBuf[i] == (byte)'\r')
                    {
                        byte[] cmdbytes = new byte[i];
                        Array.Copy(rxBuf, cmdbytes, i);
                        noUseData = new byte[count - i - 1];
                        if (noUseData.Length != 0)
                        {
                            Array.Copy(rxBuf, i + 1, noUseData, 0, count - i - 1);
                        }
                        String cmd = Encoding.Default.GetString(cmdbytes);
                        cmd = cmd.Trim();
                        return cmd;
                    }
                }
            }
            
        }
        public  void Send(string message)
        {
            try
            {
                this.socket.Send(Encoding.Default.GetBytes(message));
            }
            catch
            {
                this.connected = false;
            }
        }
    }
}
