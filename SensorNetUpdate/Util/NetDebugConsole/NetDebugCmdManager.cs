﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.Util
{
    public class NetDebugCmdManager : INetDebugCmdFactory
    {
        Dictionary<String, DelegateNetDebugCmd> dict = new Dictionary<string, DelegateNetDebugCmd>();

        public void AddDelegate(String cmd,DelegateNetDebugCmd delegateNetDebugCmd)
        {
            dict[cmd] = delegateNetDebugCmd;
        }

        #region INetDebugComFactory 成员

        public DelegateNetDebugCmd GetNetDebugCmdDelegate(string cmd)
        { 
            if (dict.ContainsKey(cmd))
            {
                return dict[cmd];
            }
            else
            {
                return null;
            }
          
        }

        #endregion

        #region INetDebugCmdFactory 成员


        public string[] GetCmdList()
        {
            String[] result = new String[dict.Count];
            dict.Keys.CopyTo(result, 0);
            return result;
          
        }

        #endregion
    }
}
