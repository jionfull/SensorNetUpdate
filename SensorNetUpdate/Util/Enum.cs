﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.Util
{
    public enum VarType
    {
        Undefined,
        Int,
        Float,
        Bool,
        String,
        Binary,
        Object,
        Long,
        DateTime
    }
}
