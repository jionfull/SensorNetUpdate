﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.Util
{
    public abstract class ByteFrameBuilder
    {
        protected ByteFrame frame=null;
        int capacty;
        protected bool buidFinish=false;
        protected bool escStatu = false;
        protected ByteDealState RxState = ByteDealState.WaitStart;
      
        
        #region 构造函数
        public ByteFrameBuilder(int capacity)
        {
            this.capacty = capacity;
        }
        public ByteFrameBuilder():this(1000)
        {

        }
        #endregion

        public ByteFrame ToByteFrame()
        {
            if (buidFinish == false) return null;
            return frame;
        }

        public void Clear()
        {
            frame = null;
            buidFinish = false;
            escStatu = false;
        }
     
        /// <summary>
        ///  返回当前帧是否构建完成
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public virtual ByteFrame Build(byte rxByte)
        {
            if (CheckEsc(rxByte))
            {
                escStatu = true;
                return null;
            }

            if (CheckStart(rxByte))
            {
                frame = new ByteFrame();
                buidFinish = false;
                RxState = ByteDealState.Reciveing;
                return null;
            }

            if (frame == null) return null;
            if (CheckEnd(rxByte))
            {
                buidFinish = true;
                RxState = ByteDealState.End;
                return frame;
            }
            if (escStatu == true)
            {
                rxByte = EscDeal(rxByte);
                escStatu = false;
            }
            frame.Append(rxByte);

            return null;
        }

        public virtual bool DoCheck()
        {
            return true;
        }
        protected abstract bool CheckStart(Byte rxByte);
        protected abstract bool CheckEnd(byte rxByte);
        /// <summary>
        /// 检查是否转义字符
        /// </summary>
        /// <param name="rxByte"></param>
        /// <returns></returns>
        protected virtual bool CheckEsc(byte rxByte)
        {
            return false;
        }

        protected virtual byte EscDeal(byte rxByte)
        {
            return rxByte;
          
        }
        
       
    }
    public class ByteFrame
    {
        private List<byte> buf;

        public List<byte> Buf
        {
            get { return buf; }
           // set { buf = value; }
        }

        public Byte[] DateBuf
        {
            get { return buf.ToArray(); }
        }
        private DateTime createTime;
        /// <summary>
        /// 类的建立时间,
        /// 解析起始帧时创建类,该时间等于接收开始的时间
        /// </summary>
        public DateTime CreateTime
        {
            get { return createTime; }
        }

        #region 构造函数
        /// <summary>
        /// 
        /// </summary>
        /// <param name="capacity">初始容量</param>
        public ByteFrame(int capacity)
        {
            createTime =DateTime.Now;

            buf = new List<byte>(capacity);   
        }
        public ByteFrame():this(1000)
        {

        }
        #endregion

        public bool Append(byte val)
        {
            buf.Add(val);
            return true;
        }

    }
    public enum ByteDealState
    {
        WaitStart,
        Reciveing,
        End,
    }
}
