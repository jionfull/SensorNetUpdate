﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.Util
{
    /// <summary>
    /// 后台处理类
    /// </summary>
    public abstract class IProcess
    {
        protected String statuText="正在处理....";


       /// <summary>
       /// 处理状态
       /// </summary>
        public String StatuText
        {
            get
            {
                return statuText;
            }
        }
        /// <summary>
        /// 处理完成
        /// </summary>
        public bool ProcessFinish
        {
            get;
            protected set;
        }
        /// <summary>
        /// 完成进度
        /// </summary>
        public float Percent
        {
            get;
            protected set;
        }
        /// <summary>
        /// 返回的数据结果,
        /// 无返回时为空
        /// 处理失败时也为空
        /// 处理过程中不确定
        /// 请检查处理完成标志
        /// </summary>
        public Object Result
        {
            get;
            protected set;
        }

        /// <summary>
        /// 进度改变事件
        /// 事件不推荐在UI中使用,因为UI中使用时会阻塞线程进度,影响后台数据的实时性
        /// </summary>
        public EventHandler<EventArgs> PercentChangeEventHandler;

        /// <summary>
        /// 状态改变事件
        /// 事件不推荐在UI中使用,因为UI中使用时会阻塞线程进度,影响后台数据的实时性
        /// </summary>
        public EventHandler<EventArgs> StatuChangeEventHandler;

        /// <summary>
        /// 处理结束事件
        /// 事件不推荐在UI中使用,因为UI中使用时会阻塞线程进度,影响后台数据的实时性
        /// </summary>
        public EventHandler<EventArgs> FinisishEventHandler;

        /// <summary>
        /// 执行进度改变事件
        /// </summary>
        /// <param name="percent">进度</param>
        protected virtual void DoPercentChange(float percent)
        {
            this.Percent = percent;
            EventHandler<EventArgs> temp = PercentChangeEventHandler;
            if (temp == null)
            {
                return;
            }
            temp(this, null);
        }

        protected virtual void DoStatuChange(String statu)
        {
            this.statuText = statu;
            EventHandler<EventArgs> temp = StatuChangeEventHandler;
            if (temp == null)
            {
                return;
            }

        }
        public IProcess()
        {
            this.Percent = 0; 
        }

        public virtual void DoFinish()
        {
          
            DoFinish("处理完成");
          
        }

        public virtual void DoFinish(String status)
        {
            DoPercentChange(100);
            DoStatuChange(status);
          
            this.ProcessFinish = true;
            EventHandler<EventArgs> temp = FinisishEventHandler;
            if (temp == null)
            {
                return;
            }
            temp(this, null);

        }
    }
}
