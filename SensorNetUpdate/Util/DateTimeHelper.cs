﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.Util
{
    public class DateTimeHelper
    {
        public static DateTime UnixTime2DateTime(uint unixtime)
        {
            return new DateTime(1970, 1, 1, 8, 0, 0).AddSeconds((double)unixtime);
        }

        public static uint DateTime2UnixTime(DateTime dt)
        {
            dt = dt.AddMilliseconds((double)-dt.Millisecond);
            DateTime dateTime = new DateTime(1970, 1, 1, 8, 0, 0);
            return Convert.ToUInt32((dt - dateTime).TotalSeconds);
        }

        public static DateTime Round(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second);
        }
    }
}
