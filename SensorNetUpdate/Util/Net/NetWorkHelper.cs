﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Timers;
using System.Threading;

namespace Lon.Util
{

    public sealed class NetworkHelper
    {
      
       


        private static NetworkHelper instance;

        /// <summary>
        /// 监听间隔
        /// </summary>
        const int LISTEN_TIME_SPAN = 2000;

        //IsNetworkAlive Description
        const int NETWORK_ALIVE_LAN = 1;
        const int NETWORK_ALIVE_WAN = 2;
        const int NETWORK_ALIVE_AOL = 4;

        const int FLAG_ICC_FORCE_CONNECTION = 1;


        private NetworkHelper()
        {

        }

        static NetworkHelper()
        {
            instance = new NetworkHelper();
        }


        public static NetworkHelper GetNetworkHelperInstance()
        {
            return instance;
        }

        /// <summary>
        /// 检查网络是否连通，有延迟
        /// </summary>
        /// <param name="connectionDescription"></param>
        /// <param name="reservedValue"></param>
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int connectionDescription, int reservedValue);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lpszUrl">连接饿地址</param>
        /// <param name="dwFlags">FLAG_ICC_FORCE_CONNECTION</param>
        /// <param name="dwReserved">0</param>
        [DllImport("wininet.dll")]
        private extern static bool InternetCheckConnection(string lpszUrl, int dwFlags, int dwReserved);

        /// <summary>
        /// 检查网络是否连通,需要启动服务
        /// </summary>
        /// <param name="connectionDescription"></param>
        [DllImport("sensapi.dll")]
        private extern static bool IsNetworkAlive(out int connectionDescription);

        /// <summary>
        /// 检查是否能建立Internet连接，VISTA不可用
        /// </summary>
        /// <param name="dest"></param>
        /// <param name="ptr">0</param>
        [DllImport("sensapi.dll")]
        private extern static bool IsDestinationReachable(string dest, IntPtr ptr);



   


      

    }
}