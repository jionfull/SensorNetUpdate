﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Lon.Util
{
    
    public class FileManager
    {
        String storeDirectory;
        int fileSize;
        int maxDays;

        public FileManager(String dir, int fileSize,int MaxDays)
        {
            this.storeDirectory = dir;
            this.fileSize = fileSize;
            this.maxDays = MaxDays;
        }

        public String GetFileName(ref DateTime dt)
        {
            String timeOutFileName = GetTimeOutFile(ref dt);
            return "";
        }

        private string GetTimeOutFile(ref DateTime dt)
        {
            String fileName = String.Format("Data{0:yyMMdd}.dat", dt.AddDays(-maxDays));
            if(File.Exists(Path.Combine(storeDirectory,fileName)))
            {
                return fileName;
            }
            else
            {
                return "";
            }
            
        }
    }
}
