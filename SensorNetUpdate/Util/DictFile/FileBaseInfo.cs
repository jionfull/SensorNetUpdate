﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Lon.Util
{
    /// <summary>
    /// 字典记录,保存一条记录的位置及长度
    /// </summary>
    public  class FileBaseInfo
    {
        /// <summary>
        /// 版本号
        /// </summary>
        public int Ver
        {
            get;
            protected set;
        }
        /// <summary>
        /// 文件大小
        /// </summary>
        public int FileSize
        {
            get;
            set;
        }
        /// <summary>
        /// 字典大小
        /// </summary>
        public int DictSize
        {
            get;
            protected set;
        }
        /// <summary>
        /// 基本信息区的大小,不论使用多少默认预留512个字节
        /// </summary>
        public int Size
        {
            get;
            private set;
        }

        /// <summary>
        /// 当前记录位置
        /// 第一个可写的位置
        /// 相对于数据区起始位置的偏移
        /// </summary>
        public int CurrWritePoint;


        public int DictCount;

        public int DataStartPos
        {
            get 
            {
                return this.Size + this.DictSize * 12;
            }
        }

        public FileBaseInfo():this(4,1024*1024*128,1024*32)
        { 
        }

        public FileBaseInfo(int fileSize, int dictSize)
            : this(4, fileSize, dictSize)
        { 
        }

        public FileBaseInfo(int ver,int fileSize,int dictSize)
        {
            this.Ver = ver;
            this.FileSize = fileSize;
            this.DictSize = dictSize;
            this.CurrWritePoint = 0;
            this.DictCount = 0;
            this.Size = 512;
        }

        public void Read(BinaryReader br)
        {
            this.Ver = br.ReadInt32();
            this.FileSize = br.ReadInt32();
            this.DictSize = br.ReadInt32();
            this.CurrWritePoint = br.ReadInt32();
            this.DictCount = br.ReadInt32();
        }

        public void Write(BinaryWriter bw)
        {
            bw.Write(this.Ver);
            bw.Write(this.FileSize);
            bw.Write(this.DictSize);
            bw.Write(this.CurrWritePoint);
            bw.Write(this.DictCount);
        }

    }
}
