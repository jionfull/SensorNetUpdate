﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Lon.Util
{
    /// <summary>
    /// 字典记录,保存一条记录的位置及长度
    /// </summary>
    public  class DictRec
    {
        public int Key;
        public int Offset;
        public int Length;
       
        public DictRec()
        { }
        public DictRec(int key, int offset, int len)
        {
            this.Key = key;
            this.Offset = offset;
            this.Length = len;
        }
        public void Write(BinaryWriter bw)
        {
            bw.Write(Key);
            bw.Write(Offset);
            bw.Write(Length);
        }
        public void Read(BinaryReader br)
        {
            Key = br.ReadInt32();
            Offset = br.ReadInt32();
            Length = br.ReadInt32();
        }
    }
}
