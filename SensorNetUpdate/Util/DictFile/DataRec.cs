﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Lon.Util
{
    public  class DataRec
    {
        /// <summary>
        /// 键值
        /// </summary>
        public int Key;

        /// <summary>
        /// 写入文件的位置
        /// </summary>
        public int Pos;

        /// <summary>
        ///  数据
        /// </summary>
        public byte[] Data;

        /// <summary>
        /// 所有数据的总长度
        /// </summary>
        public int Length
        {
            get
            {
                return this.Data.Length + 12;
            }
        }

        /// <summary>
        /// 写入数据流
        /// </summary>
        /// <param name="br"></param>
        public void Write(BinaryWriter bw)
        {
            // 可在用于文件损坏时重建字典字典
            bw.Write(Key);
            //记录中写入Pos可用于校验数据的有效性
            bw.Write(Pos);
            bw.Write(Data.Length);
            bw.Write(Data);
        }

        public void Read(BinaryReader br)
        {
            this.Key = br.ReadInt32();
            this.Pos = br.ReadInt32();
            int len = br.ReadInt32();
            this.Data = br.ReadBytes(len);
        }
    }
}
