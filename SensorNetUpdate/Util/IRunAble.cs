﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.Util
{
    public interface IRunnable
    {
        void Run();
    }
}
