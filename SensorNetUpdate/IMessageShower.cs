using System;

namespace Lon
{
    public interface IMessageShower
    {
        void ShowMessage(String type, String message);
    }
}