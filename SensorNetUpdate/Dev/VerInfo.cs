﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.Dev
{
   
    struct VerInfo
    {
        public int  FirmwareType;
        public int DevType;
        public byte MajorVer;
        public byte MinorVer;
        public DateTime RelaseData;

    }
}
