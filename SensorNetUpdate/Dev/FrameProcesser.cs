﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Lon.Dev
{
    abstract  class FrameProcesser
    {
        private CommDevice dev;
        public int PortNo;

        public int DevAddr;

        protected  AutoResetEvent WaitEvent=new AutoResetEvent(false);

        public void Reset()
        {
            WaitEvent.Reset();
        }

        public FrameProcesser(CommDevice dev)
        {
            this.dev = dev;
        }

       

        public abstract void Process(NetFrame frame);



        public void SendFrame(  byte[] data)
        {
            dev.WriteFrame(0x02, (byte)PortNo, 0, (byte)DevAddr, data);
        }

    }
}
