﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Lon
{
    class SocketReader
    {

        private Socket socket;
        public SocketReader(Socket s)
        {
            this.socket = s;
        }

        byte[] rxBuf = new byte[16 * 1024];
        int rxPoint = 0;
        int rxCount = 0;

        public byte ReadByte()
        {
            if (rxCount > rxPoint)
            {
                int point = rxPoint;
                rxPoint++;
                return rxBuf[point];
            }
            else
            {
                rxPoint = 0;
                rxCount = 0;
                rxBuf = new byte[16 * 1024];
                do
                {
                    rxCount = socket.Receive(rxBuf, 0, rxBuf.Length, SocketFlags.None);
                    if (rxCount == 0)
                    {
                        Thread.Sleep(40);
                    }
                } while (rxCount == 0);
                return ReadByte();
            }
        } 
    }
}
