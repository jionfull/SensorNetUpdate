﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms.VisualStyles;

namespace Lon.Dev
{
    class NetFrame
    {
        private byte portType;
        private byte portNo;
        private byte sensorType;
        private byte srcAddr;
        private byte dstAddr;
        private byte dataType;

        public byte[] Data;

        public byte PortType
        {
            get { return portType; }
        }

        public byte PortNo
        {
            get { return portNo; }
        }

        public byte SensorType
        {
            get { return sensorType; }
        }

        public byte SrcAddr
        {
            get { return srcAddr; }
        }

        public byte DstAddr
        {
            get { return dstAddr; }
        }

        public byte DataType
        {
            get { return dataType; }
        }


        public void Parse(byte[] rawData)
        {
            MemoryStream ms = new MemoryStream(rawData);
                BinaryReader br = new BinaryReader(ms);
            this.portType= br.ReadByte();
            this.portNo = br.ReadByte();
             
             ReadDateTime(br);
            this.sensorType = br.ReadByte();
            
            ReadDataLength(br);
            this.srcAddr = br.ReadByte();
            this.dstAddr = br.ReadByte();
            this.dataType = br.ReadByte();
          
            int remainBytesCount =(int)(ms.Length - ms.Position - 2);
          
            this.Data = br.ReadBytes(remainBytesCount);

        }

        private void ReadDataLength(BinaryReader br)
        {
            br.ReadInt16();
        }

        private void ReadDateTime(BinaryReader br)
        {
            br.BaseStream.Seek(8, SeekOrigin.Current);
        }
    }
}
