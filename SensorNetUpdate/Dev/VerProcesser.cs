﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Lon.Dev
{
    class VerProcesser : FrameProcesser
    {
        VerInfo verInfo=new VerInfo();  

        public VerProcesser(CommDevice dev) : base(dev)
        {
        }

        public override void Process(NetFrame frame)
        {
        
            if (frame.Data.Length < 6)
            {
                return;
            }
            verInfo.FirmwareType = frame.Data[0] >> 8;
            verInfo.DevType = frame.Data[0] & 0x7f;
            verInfo.MajorVer = frame.Data[1];
            verInfo.MinorVer = frame.Data[2];
            verInfo.RelaseData=new DateTime(2000+frame.Data[3],frame.Data[4],frame.Data[5]);
            base.WaitEvent.Set();

        }

        public bool ReadVerInfo(out VerInfo outVerInfo,int timeOut)
        {
            if (WaitEvent.WaitOne(timeOut, true))
            {
                outVerInfo = verInfo;
                return true;
            }
            else
            {
                outVerInfo =new VerInfo();
                return false;
            }

        }
    }
}
