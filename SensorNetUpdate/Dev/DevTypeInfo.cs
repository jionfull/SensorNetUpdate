﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Lon.Dev
{
   public class DevTypeInfo
    {
        [XmlAttribute("编号")]
        public int Id;

        [XmlAttribute("名称")]
        public String Name;


        [XmlAttribute("固件类型")]
        public int FirmwareId=-1;
        [XmlAttribute("升级单次字节数")]
        public int FirmwareUpdateSpan;



       
    }
}
