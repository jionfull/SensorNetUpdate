﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using Lon.Util;

namespace Lon.Dev
{
    [XmlRoot("智能传感器设备类型表")]
    public class DevTypeManager
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("设备类型")]
        public List<DevTypeInfo> TypeList=new List<DevTypeInfo>();

        public static DevTypeManager Read(String path)
        {
            using (FileStream stream = File.Open(path, FileMode.Open))
            {
                XmlSerializer serializer=new XmlSerializer(typeof(DevTypeManager));
                return (DevTypeManager) serializer.Deserialize(stream);
            }
        }

        public static bool Write(DevTypeManager typeManager, String path)
        {
            using (FileStream stream = File.Open(path, FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(DevTypeManager));
                serializer.Serialize(stream,typeManager);
                return true;
            }
            
        }

        public static bool TstWrite()
        {
            String path = PathHelper.GetApplicationPath() + "\\DevTypeConfig.xml";
            DevTypeManager devTypeManager=new DevTypeManager();
            DevTypeInfo dti=new DevTypeInfo();
            dti.Name = "道岔表示";
            dti.Id = 16;
            dti.FirmwareUpdateSpan = 512;
            devTypeManager.TypeList.Add(dti);
            return Write(devTypeManager, path);
        }
    }

}
