﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Lon.Dev
{
    class FileUpdateProcesser : FrameProcesser
    {

        private int lastFrameIndex = -1;
        private int lastDevType = -1;
        private int result=-1;

        public FileUpdateProcesser(CommDevice dev) : base(dev)
        {
        }

        public override void Process(NetFrame frame)
        {
           
            MemoryStream ms=new MemoryStream(frame.Data);
            BinaryReader br =new BinaryReader(ms);
           
            lastFrameIndex = br.ReadUInt16();
            lastDevType = br.ReadByte();
            result = br.ReadByte();

            base.WaitEvent.Set();

        }

        public bool ReadResult(out int frameId,out int result,int timeOut)
        {
            if (WaitEvent.WaitOne(timeOut, true))
            {
                frameId = this.lastDevType;
                result = this.result;
                return true;
            }
            else
            {
                frameId = -1;
                result = -1;
                return false;
            }

        }
    }
}
