﻿using System;

namespace Lon
{
    public interface IRawDataShower
    {
        void ShowRawData(String dir, Byte[] data);
    }
}